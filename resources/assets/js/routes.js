import Home from './components/Home.vue';
import Login from './components/auth/Login.vue';
import Register from './components/auth/Register.vue';
import GroupsMain from './components/groups/Main.vue';
import GroupsList from './components/groups/List.vue';
import NewGroup from './components/groups/New.vue';
import Group from './components/groups/View.vue';


export const routes = [
    {
        path: '/',
        component: Home,
        meta:{
            requiresAuth: true
        }
    },

    {
        path: '/login',
        component: Login
    },

    {
        path: '/register',
        component: Register
    },
    {
        path: '/groups',
        component: GroupsMain,
        meta:{
            requiresAuth: true
        },
        children:[
            {
                path: '/',
                component: GroupsList
            },
            {
                path: '/new',
                component: NewGroup
            },
            {
                path: ':id',
                component: Group
            },

            ]
    }
];