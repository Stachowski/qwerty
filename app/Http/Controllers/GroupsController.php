<?php

namespace App\Http\Controllers;

use App\Group;
use App\Http\Requests\CreateGroupRequest;



class
GroupsController extends Controller
{
    public function all()
    {
        $groups = Group::all();

        return response()->json([
            "groups" => $groups
        ], 200);
    }

    public function get($id)
    {
        $group = Group::whereId($id)->first();

        return response()->json([
            "group" => $group
        ], 200);
    }

    public function new(CreateGroupRequest $request)
    {
        $group = Group::create($request->only(["name", "description"]));

        return response()->json([
            "group" => $group
        ], 200);
    }
}
